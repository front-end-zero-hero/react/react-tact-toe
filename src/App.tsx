import './App.css'
import Player from "./components/Player.tsx";
import GameBoard from "./components/GameBoard.tsx";
import {useState} from "react";
import Log from "./components/Log.tsx";
import {GameTurn} from "./model/gameTurn.ts";
import {WINNING_COMBINATIONS} from "./model/winning-combination.ts";
import GameOver from "./components/GameOver.tsx";

const INITIAL_GAME_BOARD: (string | null)[][] = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
];

const PLAYERS = {
    X: 'PLayer 1',
    O: 'Player 2'
}

const deriveActivePlayer = (gameTurn: GameTurn[]) =>  {
    let currentPlayer = 'X';
    if (gameTurn.length > 0 && gameTurn[0].player === 'X') {
        currentPlayer = 'O';
    }

    return currentPlayer;
}

const deriGameBoard = (gameTurns: GameTurn[]) => {
    const gameBoard = [...INITIAL_GAME_BOARD.map(array => [...array])];

    for(const turn of gameTurns) {
        const { square, player } = turn;
        const {row, col} = square;
        gameBoard[row][col] = player;
    }

    return gameBoard;
}

const deriveWinner = (gameBoard: (string|null)[][], players: {[key:string]: string}) => {
    let winner;
    for (const combination of WINNING_COMBINATIONS) {
        const firstSquareSymbol = gameBoard[combination[0].row][combination[0].column];
        const secondSquareSymbol = gameBoard[combination[1].row][combination[1].column];
        const thirdSquareSymbol = gameBoard[combination[2].row][combination[2].column];

        if (firstSquareSymbol !== null && firstSquareSymbol === secondSquareSymbol && firstSquareSymbol === thirdSquareSymbol) {
            winner = players[firstSquareSymbol];
        }
    }

    return winner;
}

const App = () => {
    const [players, setPlayers] = useState<{[key:string]: string}>(PLAYERS)
    const [gameTurns, setGameTurns] = useState<GameTurn[]>([]);
    // const [hasWinner, setHasWinner] = useState(false);
    const activePlayer = deriveActivePlayer(gameTurns);

    const gameBoard = deriGameBoard(gameTurns);

    const winner = deriveWinner(gameBoard, players);

    const hasDraw = gameTurns.length == 9 && !winner;

    const handleSelectSquare = (rowIndex: number, colIndex: number) => {
        setGameTurns(preTurn => {
            const currentPlayer = deriveActivePlayer(preTurn);

            const updateTurns: GameTurn[] = [{
                square:
                    {row: rowIndex, col: colIndex}, player: currentPlayer
            }
                , ...preTurn
            ];

            return updateTurns;
        })
    }

    const handleRematch = () => {
        setGameTurns([]);
    }

    const handlePlayerNameChange = (symbol:string, newName:string) => {
        setPlayers(prevPlayers => {
            return {
                ...prevPlayers,
                [symbol]: newName
            }
        })
    }

    return (
        <main>
            <div id="game-container">
                <ol id="players" className="highlight-player">
                    <Player initialName={PLAYERS.X} symbol="X" isActive={activePlayer === 'X'} onChangeName={handlePlayerNameChange}/>
                    <Player initialName={PLAYERS.O} symbol="O" isActive={activePlayer === 'O'} onChangeName={handlePlayerNameChange}/>
                </ol>
                {(winner || hasDraw)  && <GameOver winner={winner!} onRestart={handleRematch}/>}
                <GameBoard
                    onSelectSquare={handleSelectSquare}
                    board={gameBoard}
                />
            </div>
            <Log turns={gameTurns}/>
        </main>
    )
}

export default App
