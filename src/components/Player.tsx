import React, {ReactElement, useState} from "react";

interface IProps {
    initialName: string;
    symbol: string;
    isActive: boolean,
    onChangeName: (symbol:string, playerName:string) => void
}

const Player: React.FC<IProps> = ({initialName, symbol, isActive, onChangeName}): ReactElement => {
    const [playName, setPlayName] = useState(initialName);
    const [isEditing, setEditing] = useState(false);

    const onEdit = () => {
        setEditing(prev => !prev);
        if(isEditing) {
            onChangeName(symbol, playName);
        }
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPlayName(event.target.value)
    }

    return (
        <li className={isActive ? 'active' : undefined}>
            <span className="player">
                {isEditing ?
                    (<input type="text" className="player-name" required value={playName} onChange={handleChange}/>):
                    (<span className="player-name">{playName}</span>)
                }
                <span className="player-symbol">{symbol}</span>
            </span>
            <button onClick={onEdit}>{isEditing ? 'Save' : 'Edit'}</button>
        </li>
    )
}

export default Player;
